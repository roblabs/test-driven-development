package at.inaut.hw;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

public class HelloWorldActivity extends Activity {
	/** Called when the activity is first created. */

    private static final String LOG_TAG = "HelloWorldActivity";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        Log.d(LOG_TAG, "onCreate()");
//		setContentView(R.layout.main);
	}

    @Override
    public void onStart()
    {
        Log.d(LOG_TAG, "onStart()");
        super.onStart();
    }

    @Override
    public void onStop()
    {
        Log.d(LOG_TAG, "onStop()");
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        Log.d(LOG_TAG, "onDestroy()");
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        Log.d(LOG_TAG, "onPause()");
        super.onPause();
    }

    @Override
    protected void onResume() {
        Log.d(LOG_TAG, "onResume()");
        super.onResume();
    }
}
