package at.inaut.hw;

import android.app.Activity;
import android.widget.TextView;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.junit.Assert.*;

import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;

@RunWith(RobolectricTestRunner.class)
public class HelloWorldTest {
	@Test
	public void testAndroidActivityInstantiation() {
        System.out.println("testAndroidActivityInstantiation");
        Activity activity = Robolectric.buildActivity(HelloWorldActivity.class)
                .create()
                .start()
                .resume()
                .visible()
                .get();
	}

    @Test
    public void testTextView() {

        System.out.println("testTextView");

        Activity activity = Robolectric.buildActivity(HelloWorldActivity.class)
                .create()
                .get();

        TextView tv = new TextView(activity);
        tv.setText("Rob");

        assertEquals("Rob", tv.getText());
    }

    @Test
    public void testSum() {
        int a = 5;
        int b = 10;
        int result = a + b;
        System.out.println("testSum");
        assertEquals(15, result);
    }

    @Ignore("Test is ignored as a demonstration")
    @Test
    public void testThatIsIgnored() {
        
    }
    
    // this test will succeed as it does raise the expected exception
    @Test(expected = IndexOutOfBoundsException.class)
    public void testException()
    {
        int[] array = new int[] { 1, 2 };
        System.out.println("Last item in array: " + array[2]);
    }
    
    @Test
    public void testAssertEquals() {
        org.junit.Assert.assertEquals("failure - strings not same", 5l, 5l);
    }
    
}
