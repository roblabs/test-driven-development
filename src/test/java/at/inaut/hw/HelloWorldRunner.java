package at.inaut.hw;

import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.junit.runners.model.InitializationError;

import java.io.File;

public class HelloWorldRunner extends RobolectricTestRunner {
	public HelloWorldRunner(Class testClass) throws InitializationError {
		super(testClass);
	}
}
