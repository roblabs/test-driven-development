package com.roblabs.android.junit;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;


@RunWith(Parameterized.class)
public class SquaredIntegerTest{

    private int fInput;
    private int fExpected;

    public SquaredIntegerTest(int input, int expected) {
        fInput= input;
        fExpected= expected;
    }

    @Before
    public void setUp() throws Exception {
        System.out.println("@Before ");
    }

    @After
    public void tearDown() throws Exception {
        System.out.println("@After");
        System.out.println("");
    }

    @Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
//            index, squared
//                { 0, 0 },  // uncomment to see behavior of assertNotSame
//                { 1, 1 },
                { 2, 4 },
                { 3, 9 },
                { 4, 16 },
                { 5, 25 },
                { 6, 36 },
                { 7, 49 },
                { 8, 64 },
                { 9, 81 },
        });
    }

    @Test
    public void testSquared() {
        assertEquals(fExpected, fInput * fInput);
        System.out.println("testSquared  input = " + fInput + ", x * x = " + fExpected);
    }

    @Test
    public void testCubed() {
        assertNotSame(fExpected, fInput * fInput * fInput);
        System.out.println("testCubed    input = " + fInput + ",x * x * x = " + fExpected);
    }

}