package com.roblabs.android.junit;

import java.util.Collection;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;


@RunWith(Parameterized.class)
public class Vector1DDataTest{

    private int mInput;
    private int mSum = 0;

    public Vector1DDataTest(int input) {
        mInput = input;
    }

    @Before
    public void setUp() throws Exception {
        System.out.println("@Before ");
    }

    @After
    public void tearDown() throws Exception {
        System.out.println("@After");
        System.out.println("");
    }

    @Parameters
    public static Collection<Object[]> data() {
        Object[][] data = new Object[][] {
                { 1 },
                { 5 },
                { 10 }
        };
        return Arrays.asList(data);
    }

    @Test
    public void testListData() {
        mSum += mInput;
        System.out.println("testListData  input = " + mInput);
        System.out.println("testListData  mSum  = " + mSum);
    }

}