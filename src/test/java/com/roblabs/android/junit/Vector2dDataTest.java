package com.roblabs.android.junit;

import java.lang.Integer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;


@RunWith(Parameterized.class)
public class Vector2dDataTest{

    private int mInput;
    private Object[] mList;

    public Vector2dDataTest(Object[] list) {
        mList = list;
    }

    @Before
    public void setUp() throws Exception {
        System.out.println("@Before ");
    }

    @After
    public void tearDown() throws Exception {
        System.out.println("@After");
        System.out.println("");
    }

    @Parameters
    public static Collection<Object[][]> data() {

        Object[][][] data = new Object[][][] {
                {
                        {1.1, 1.2}
                },

                {
                        {2.1, 2.2, 2.3, 2.4}
                },
        };
        return Arrays.asList(data);

    }

    @Test
    public void testListData() {
        System.out.println("testListData input = " + mList[0] + ", length = " + mList.length);
    }
}